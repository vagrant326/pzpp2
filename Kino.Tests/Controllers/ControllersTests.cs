﻿using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Kino.Controllers;
using Kino.Models;
using System.Linq;

namespace Kino.Tests.Controllers
{
    [TestClass]
    public class ControllersTests
    {
        [TestMethod]
        public void SalaIndexTest()
        {
            SalaController controller = new SalaController();

            ViewResult result = controller.Index() as ViewResult;

            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void AspNetUsersIndexTest()
        {
            AspNetUsersController controller = new AspNetUsersController();

            ViewResult result = controller.Index() as ViewResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void AspNetUserRolesIndexTest()
        {
            AspNetUserRolesController controller = new AspNetUserRolesController();

            ViewResult result = controller.Index() as ViewResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void AspNetRolesIndexTest()
        {
            AspNetRolesController controller = new AspNetRolesController();

            ViewResult result = controller.Index() as ViewResult;

            Assert.IsNotNull(result);
        }


        [TestMethod]
        public void AktualnosciIndexTest()
        {
            HomeController controller = new HomeController();

            ViewResult result = controller.Index() as ViewResult;

            Assert.IsNotNull(result);
        }

    }
}
