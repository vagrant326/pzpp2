﻿using System.ComponentModel.DataAnnotations;

namespace Kino.Models
{
    public class Rezerwacja
    {
        [Key]
        public int IdRezerwacji { get; set; }

        [Required]
        [StringLength(30, ErrorMessage = "Imie nie zawierac wiecej niz 30 znakow")]
        [RegularExpression(@"^[A-Z]{1}[^\s\d\W_]{3,30}$")]
        [Display(Name = "Imię")]
        public string Imie { get; set; }

        [Required]
        [StringLength(40, ErrorMessage = "Nazwisko nie moze zawierac wiecej niz 40 znakow")]
        [RegularExpression(@"^[A-Z]{1}[^\s\d\W_]{3,20}[\-]{1}[^\s\d\W_]{3,20}$")]
        public string Nazwisko { get; set; }

        [Required]
        [DataType(DataType.EmailAddress, ErrorMessage = "Nieprawidlowy adres e-mail")]
        [Display(Name = "E-Mail")]
        public string Email { get; set; }

        [Required]
        [RegularExpression(@"^\d{3}-\d{3}-\d{3}$", ErrorMessage = "Numer telefonu musi byc w formacie: xxx-yyy-zzz")]
        [StringLength(11)]
        [Display(Name = "Numer telefonu")]
        public string NrTelefonu { get; set; }

        [Required]
        public virtual Seans seans { get; set; }

        [Required]
        [Display(Name = "Ilosc biletów")]
        [Range(1, 40)] // MAX'ymalna ilosc osob zamawiajacych jedna rezerwacje
        public int IloscBiletow { get; set; }
        // jakie bilety -ulgowe etc.
        // ktore miejsca
    }
}