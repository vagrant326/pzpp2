﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Kino.Models
{
    public class Kontakt
    {
        [Display(Name = "Adres")]
        public string Adres { get; set; }

        [DataType(DataType.EmailAddress, ErrorMessage = "Nieprawidlowy adres e-mail")]
        [Display(Name = "E-Mail")]
        public string Email { get; set; }

        [Display(Name = "Numer telefonu")]
        public string NrTelefonu { get; set; }

        [Display(Name = "Faks")]
        public string Fax { get; set; }

        [Display(Name = "Godziny otwarcia")]
        public string GodzinyOtwarcia { get; set; }
    }
}