﻿using System.Data.Entity;

namespace Kino.Models
{
    public class Konteksty : DbContext
    {
        public Konteksty(): base("DefaultConnection")
        {
        }
        
        public DbSet<Film> Filmy { get; set; }
        public DbSet<Rezerwacja> Rezerwacja { get; set; }
        public DbSet<Sala> Sala { get; set; }
        public DbSet<Seans> Seans { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<Aktualnosci> Aktualnosci { get; set; }
        public DbSet<Cennik> Cennik { get; set; }
        public DbSet<Projekt> Projekty { get; set; }
        public DbSet<MiejscaRezerwacji> Miejsca { get; set; }
    }
}