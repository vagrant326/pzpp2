﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Kino.Models
{
    public class Projekt
    {
        [Key]
        public int ProjektId { get; set; }
        [Required]
        public string Nazwa { get; set; }
        [Required]
        [Column(TypeName = "nvarchar(MAX)")]
        public string Opis { get; set; }
        [Required]
        [Display(Name = "Adres obrazka")]
        public string AdresGrafiki { get; set; }
    }
}