﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Kino.Models
{
    public class Cennik
    {
        public int Id { get; set; }

        [Display(Name="Rodzaj biletu")]
        public string RodzajBiletu { get; set; }

        [Display(Name = "Pn - Czw (2D)")]
        public int PnCzw2D { get; set; }

        [Display(Name = "Pt - Nd (2D)")]
        public int PtNd2D { get; set; }

        [Display(Name = "Pn - Czw (3D)")]
        public int PnCzw3D { get; set; }

        [Display(Name = "Pt - Nd (3D)")]
        public int PtNd3D { get; set; }
    }
}