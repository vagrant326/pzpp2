﻿using System.ComponentModel.DataAnnotations;

namespace Kino.Models
{
    public class Sala
    {
        [Key]
        public int Numer { get; set; }

        [Required]
        [Range(100, 600)] // Pojemnosc sali MIN MAX
        [Display(Name = "Pojemność sali")]
        public int Pojemnosc { get; set; }

        [Required]
        [Range(0, 20)] // Miejsca dla niepelnosprawnych MIN MAX
        [Display(Name = "Ilość miejsc dla niepełnosprawnych")]
        public int Niepelnosprawni { get; set; }

        [Required]
        [Range(10, 30)]
        [Display(Name = "Ilość rzędów")]
        public int IloscRzedow { get; set; }
    }
}