﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Kino.Models
{
    public class MiejscaRezerwacji
    {
        [Key]
        public int IdMiejscaRezerwacji { get; set; }
        [Display(Name = "Numer miejsca")]
        public int NumerMiejsca { get; set; }
        [Display(Name = "ID rezerwacji")]
        public virtual Rezerwacja RezerwacjaId { get; set; }
    }
}