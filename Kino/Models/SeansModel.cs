﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Kino.Models
{
    public class Seans
    {
        [Key]
        public int IdSeansu { get; set; }

        [Required]
        public virtual Film film { get; set; }

        [Required]
        public DateTime data { get; set; }

        [Required]
        public virtual Sala sala { get; set; }

        //public static void StworzTabele()
        //{
        //    using (var db = new Konteksty())
        //    {
        //        try
        //        {
        //            var dane = from x in db.Seans select x;
        //        }
        //        catch (SqlException)
        //        {
        //            db.Dispose();
        //            throw new DbUpdateException();
        //        }
        //    }
        //}

    }
}