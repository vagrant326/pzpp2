﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kino.Models
{
    public class Film
    {
        [Key]
        public int IdFilmu { get; set; }

        [Required]
        [Display(Name="Tytuł")]
        public string Tytul { get; set; }

        [Required]
        [Display(Name = "Data premiery")]
        public DateTime DataPremiery { get; set; }

        [Required]
        [Column(TypeName="nvarchar(MAX)")]
        [Display(Name = "Opis")]
        public string Opis { get; set; }

        [Display(Name = "Link do YouTube")]
        public string LinkYouTube { get; set; }

        [Display(Name = "Link do plakatu")]
        public string LinkDoZdjecia { get; set; }

        [Required]
        [Display(Name = "Dozwolony od lat")]
        public int WiekWidza { get; set; }

        [Required]
        [EnumDataType(typeof(Wersja))]
        [Display(Name = "Wersja filmu")]
        public Wersja WersjaFilmu { get; set; }

        [Required]
        [Display(Name = "Język")]
        public string Jezyk { get; set; }

        [Required]
        [Display(Name = "Długość")]
        public int Dlugosc { get; set; }

        [Required]
        public string Gatunek { get; set; }

        [Required]
        [Display(Name = "Oryginalny tytuł")]
        public string OryginalnyTytul { get; set; }

        [Required]
        [Display(Name = "Reżyser")]
        public string Rezyser { get; set; }

        [Required]
        public string Obsada { get; set; }

        [Required]
        public string Produkcja { get; set; }

        [Required]
        [Display(Name = "Rok produkcji")]
        public string RokProdukcji { get; set; }
    }

    public enum Wersja
    {
        Dubbing,
        Lektor,
        NapisyPL,
        Oryginał,
        FilmPolski
    }
}