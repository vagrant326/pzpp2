﻿using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Kino.Models;
using Kino.Services;
using Kino.Repositories;

namespace Kino.Controllers
{
    public class FilmController : Controller
    {
        private Konteksty db = new Konteksty();

        [Autoryzuj(Role = Role.Admin | Role.Menadzer | Role.Planista | Role.Kasjer)]
        public ActionResult PrzelaczTryb(bool? pracownik)
        {
            if (pracownik==true)
            {
                return View("Admin/Index", db.Filmy.ToList());
            }
            else
            {
                return View("Index", db.Filmy.ToList());
            }
        }
               
        //
        // GET: /Default1/
        [AllowAnonymous]
        public ActionResult Index()
        {
            ViewBag.Title = "Filmy";
            if (User.IsInRole(Role.Admin | Role.Menadzer | Role.Planista | Role.Kasjer))
            {
                return View("Admin/Index",db.Filmy.ToList());
            }
            else
            {
                return View(db.Filmy.ToList());
            }
        }

        //
        // GET: /Default1/Details/5
        [Autoryzuj(Role = Role.Admin|Role.Menadzer|Role.Planista|Role.Kasjer)]
        public ActionResult Details(int id = 0)
        {
            Film film = db.Filmy.Find(id);
            if (film == null)
            {
                return HttpNotFound();
            }
            return View("Admin/Details",film);
        }

        //
        // GET: /Default1/Create
        [Autoryzuj(Role = Role.Admin | Role.Menadzer)]
        public ActionResult Create()
        {
            return View("Admin/Create");
        }

        //
        // POST: /Default1/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Autoryzuj(Role = Role.Admin | Role.Menadzer | Role.Planista)]
        public ActionResult Create(Film film)
        {
            if (ModelState.IsValid)
            {
                db.Filmy.Add(film);
                db.SaveChanges();
                return RedirectToAction("Admin/Index");
            }

            return View("Admin/Create",film);
        }

        //
        // GET: /Default1/Edit/5
        [Autoryzuj(Role = Role.Admin | Role.Menadzer)]
        public ActionResult Edit(int id = 0)
        {
            Film film = db.Filmy.Find(id);
            if (film == null)
            {
                return HttpNotFound();
            }
            return View("Admin/Edit",film);
        }

        //
        // POST: /Default1/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Autoryzuj(Role = Role.Admin | Role.Menadzer)]
        public ActionResult Edit(Film film)
        {
            if (ModelState.IsValid)
            {
                db.Entry(film).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View("Admin/Edit",film);
        }

        //
        // GET: /Default1/Delete/5

        [Autoryzuj(Role = Role.Admin)]
        public ActionResult Delete(int id = 0)
        {
            Film film = db.Filmy.Find(id);
            if (film == null)
            {
                return HttpNotFound();
            }
            return View("Admin/Delete",film);
        }

        //
        // POST: /Default1/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Autoryzuj(Role = Role.Admin)]
        public ActionResult DeleteConfirmed(int id)
        {
            Film film = db.Filmy.Find(id);
            db.Filmy.Remove(film);
            db.SaveChanges();
            return RedirectToAction("Admin/Index");
        }
        [HttpPost]
        public JsonResult PobierzFilmy()
        {
            return Json(new SqlFilmRepozytorium().ZwrocWszystkie(), JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}