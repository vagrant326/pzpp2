﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Kino.Models;
using Kino.Services;

namespace Kino.Controllers
{
    public class ProjektsController : Controller
    {
        private Konteksty db = new Konteksty();
        [Autoryzuj(Role = Role.Admin | Role.Menadzer | Role.Planista | Role.Kasjer)]
        public ActionResult PrzelaczTryb(bool? pracownik)
        {
            if (pracownik == true)
            {
                return View("Admin/Index", db.Projekty.ToList());
            }
            else
            {
                return View("Index", db.Projekty.ToList());
            }
        }
        // GET: Projekts
        [AllowAnonymous]
        public ActionResult Index()
        {
            ViewBag.Title = "Projekty";

            if (User.IsInRole(Role.Admin | Role.Menadzer | Role.Planista | Role.Kasjer))
            {
                return View("Admin/Index", db.Projekty.ToList());
            }
            else
            {
                return View(db.Projekty.ToList());
            }

        }
        // GET: Projekts/Details/5
        [Autoryzuj(Role = Role.Admin | Role.Menadzer | Role.Planista | Role.Kasjer)]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Projekt projekt = db.Projekty.Find(id);
            if (projekt == null)
            {
                return HttpNotFound();
            }
            return View(projekt);
        }

        // GET: Projekts/Create
        [Autoryzuj(Role = Role.Admin | Role.Menadzer)]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Projekts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Autoryzuj(Role = Role.Admin | Role.Menadzer)]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ProjektId,Nazwa,Opis,AdresGrafiki")] Projekt projekt)
        {
            if (ModelState.IsValid)
            {
                db.Projekty.Add(projekt);
                db.SaveChanges();
                return RedirectToAction("Admin/Index");
            }

            return View(projekt);
        }

        // GET: Projekts/Edit/5
        [Autoryzuj(Role = Role.Admin | Role.Menadzer)]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Projekt projekt = db.Projekty.Find(id);
            if (projekt == null)
            {
                return HttpNotFound();
            }
            return View(projekt);
        }

        // POST: Projekts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Autoryzuj(Role = Role.Admin | Role.Menadzer)]
        public ActionResult Edit([Bind(Include = "ProjektId,Nazwa,Opis,AdresGrafiki")] Projekt projekt)
        {
            if (ModelState.IsValid)
            {
                db.Entry(projekt).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Admin/Index");
            }
            return View(projekt);
        }

        // GET: Projekts/Delete/5
        [Autoryzuj(Role = Role.Admin | Role.Menadzer)]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Projekt projekt = db.Projekty.Find(id);
            if (projekt == null)
            {
                return HttpNotFound();
            }
            return View(projekt);
        }

        // POST: Projekts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Autoryzuj(Role = Role.Admin | Role.Menadzer)]
        public ActionResult DeleteConfirmed(int id)
        {
            Projekt projekt = db.Projekty.Find(id);
            db.Projekty.Remove(projekt);
            db.SaveChanges();
            return RedirectToAction("Admin/Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
