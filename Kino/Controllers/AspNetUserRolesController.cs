﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Kino.Models;
using Kino.Services;

namespace Kino.Controllers
{
    public class AspNetUserRolesController : Controller
    {
        private User db = new User();

        // GET: AspNetUserRoles
        [Autoryzuj(Role = Role.Admin | Role.Menadzer)]
        public ActionResult Index()
        {
            var aspNetUserRoles = db.AspNetUserRoles.Include(a => a.AspNetRoles);
            return View(aspNetUserRoles.ToList());
        }

        // GET: AspNetUserRoles/Details/5
        [Autoryzuj(Role = Role.Admin | Role.Menadzer)]
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AspNetUserRoles aspNetUserRoles = (from x in db.AspNetUserRoles where x.UserId == id select x).First();
            if (aspNetUserRoles == null)
            {
                return HttpNotFound();
            }
            return View(aspNetUserRoles);
        }

        // GET: AspNetUserRoles/Create
        [Autoryzuj(Role = Role.Admin | Role.Menadzer)]
        public ActionResult Create()
        {
            ViewBag.RoleId = new SelectList(db.AspNetRoles, "Id", "Name");
            return View();
        }

        // POST: AspNetUserRoles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Autoryzuj(Role = Role.Admin | Role.Menadzer)]
        public ActionResult Create([Bind(Include = "UserId,RoleId")] AspNetUserRoles aspNetUserRoles)
        {
            if (ModelState.IsValid)
            {
                db.AspNetUserRoles.Add(aspNetUserRoles);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.RoleId = new SelectList(db.AspNetRoles, "Id", "Name", aspNetUserRoles.RoleId);
            return View(aspNetUserRoles);
        }

        // GET: AspNetUserRoles/Delete/5
        [Autoryzuj(Role = Role.Admin | Role.Menadzer)]
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AspNetUserRoles aspNetUserRoles = (from x in db.AspNetUserRoles where x.UserId == id select x).First();
            if (aspNetUserRoles == null)
            {
                return HttpNotFound();
            }
            return View(aspNetUserRoles);
        }

        // POST: AspNetUserRoles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Autoryzuj(Role = Role.Admin | Role.Menadzer)]
        public ActionResult DeleteConfirmed(string id)
        {
            AspNetUserRoles aspNetUserRoles = (from x in db.AspNetUserRoles where x.UserId == id select x).First();
            db.AspNetUserRoles.Remove(aspNetUserRoles);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
