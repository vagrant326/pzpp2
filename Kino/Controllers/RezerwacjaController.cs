﻿using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Kino.Models;
using Kino.Services;
using System;
using System.Collections.Generic;
using System.Collections;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Converters;

namespace Kino.Controllers
{
    using Repositories;

    public class RezerwacjaController : Controller
    {
        private Konteksty db = new Konteksty();

        //
        // GET: /Rezerwacja/

        public ActionResult Index()
        {
            if (User.IsInRole(Role.Admin | Role.Menadzer | Role.Planista | Role.Kasjer))
            {
                return View("Admin/Index", db.Rezerwacja.ToList());
            }
            else
            {
                return View(db.Rezerwacja.ToList());
            }
        }

        //
        // GET: /Rezerwacja/Details/5

        public ActionResult Details(int id = 0)
        {
            Rezerwacja Rezerwacja = db.Rezerwacja.Find(id);
            if (Rezerwacja == null)
            {
                return HttpNotFound();
            }
            return View("Admin/Details", Rezerwacja);
        }

        //
        // GET: /Rezerwacja/Create

        public ActionResult Create()
        {
            return View("Admin/Create");
        }

        //
        // POST: /Rezerwacja/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Rezerwacja Rezerwacja)
        {
            if (ModelState.IsValid)
            {
                db.Rezerwacja.Add(Rezerwacja);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View("Admin/Create",Rezerwacja);
        }

        //
        // GET: /Rezerwacja/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Rezerwacja Rezerwacja = db.Rezerwacja.Find(id);
            if (Rezerwacja == null)
            {
                return HttpNotFound();
            }
            return View("Admin/Edit", Rezerwacja);
        }

        //
        // POST: /Rezerwacja/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Rezerwacja Rezerwacja)
        {
            if (ModelState.IsValid)
            {
                db.Entry(Rezerwacja).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View("Admin/Edit", Rezerwacja);
        }

        //
        // GET: /Rezerwacja/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Rezerwacja Rezerwacja = db.Rezerwacja.Find(id);
            if (Rezerwacja == null)
            {
                return HttpNotFound();
            }
            return View("Admin/Delete", Rezerwacja);
        }

        //
        // POST: /Rezerwacja/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Rezerwacja Rezerwacja = db.Rezerwacja.Find(id);
            db.Rezerwacja.Remove(Rezerwacja);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult PobierzZarezerwowaneMiejsca(FormCollection formCollection)
        {
            ADONetSupport ado = ADONetSupport.CreateInstance();
            ado.OpenConnection();

            object[][] rows = ado.GetObjectRow("SELECT IdRezerwacji, seans_IdSeansu FROM Rezerwacjas", new string[] { "IdRezerwacji", "seans_IdSeansu" });
            ado.CloseConnection();
            
            int seansID = Convert.ToInt32(formCollection["seansID"]);

            List<int> rezerw = new List<int>();
            for (int i = 0; i < rows.Length; i++)
			{
                object[] row = rows[i];
                int seanseID = (int)row[1];
                int rezID = (int)row[0];
                if (seanseID == seansID)
                {
                    rezerw.Add(rezID);
                }
			}

            SqlMiejscaRezerwacji repozytoriumMiejscRezerwacji = new SqlMiejscaRezerwacji();
            List<MiejscaRezerwacji> miejscaRezerwacji = repozytoriumMiejscRezerwacji.ZwrocWszystkie();
            List<int> zajetePozycje = new List<int>();
            ado.OpenConnection();
            object[][] mr = ado.GetObjectRow("SELECT RezerwacjaId_IdRezerwacji, NumerMiejsca FROM MiejscaRezerwacjis", new string[] { "NumerMiejsca", "RezerwacjaId_IdRezerwacji" });
            ado.CloseConnection();
            for (int i = 0; i < mr.Length; i++)
            {
                object[] row = mr[i];
                int numerMiejsca = (int)row[0];
                int rezerwacID = (int)row[1];
                //System.Windows.Forms.MessageBox.Show(rezerwacID+":"+numerMiejsca);
                for (int j = 0; j < rezerw.Count; j++)
                {
                    if (rezerw.ElementAt(j) == rezerwacID)
                        zajetePozycje.Add(numerMiejsca);
                }
            }
            

            /*for (int i = 0; i < miejscaRezerwacji.Count; i++)
            {
                System.Windows.Forms.MessageBox.Show(miejscaRezerwacji.ElementAt(i).RezerwacjaId.IdRezerwacji.ToString());
                for (int j = 0; j < rezerw.Count; j++)
                {
                    if (miejscaRezerwacji.ElementAt(i).RezerwacjaId.IdRezerwacji == rezerw.ElementAt(j))
                    {
                        zajetePozycje.Add(miejscaRezerwacji.ElementAt(i).NumerMiejsca);
                    }
                }
            }*/

            return Json(zajetePozycje, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Rezerwuj(FormCollection formCollection)
        {
            // AJAX wywala mi blad ale jak debuguje tutaj to lapie mi normalnie FormCollection wiec nie wiem co z tym zrobic
            // Tutaj zapisac rezerwacje w bazie
            Rezerwacja rezerwacja = new Rezerwacja();
            SqlRezerwacjaRepozytorium repozytoriumRezerwacji = new SqlRezerwacjaRepozytorium();
            SqlMiejscaRezerwacji repozytoriumMiejscRezerwacji = new SqlMiejscaRezerwacji();

            string imie = "", nazwisko = "", email = "", telefon = "";
            int seansID = 0;
            string miejscaStr = "";

            foreach (string keyName in formCollection)
            {
                if (keyName == "Imie")
                    imie = formCollection[keyName];

                if (keyName == "Nazwisko")
                    nazwisko = formCollection[keyName];

                if (keyName == "Email")
                    email = formCollection[keyName];

                if (keyName == "Telefon")
                    telefon = formCollection[keyName];

                if (keyName == "Seans")
                    seansID = Convert.ToInt32(formCollection[keyName]);

                if (keyName == "Miejsca")
                    miejscaStr = formCollection[keyName];
            }

            string[] miejscaStrSpl = miejscaStr.Split(new char[]{'.'}, StringSplitOptions.RemoveEmptyEntries);
            int[] miejsca = new int[miejscaStrSpl.Length];
            for (int i = 0; i < miejscaStrSpl.Length; i++)
                miejsca[i] = Convert.ToInt32(miejscaStrSpl[i]);

            List<Rezerwacja> wszystkieRezerwacje = repozytoriumRezerwacji.ZwrocWszystkie();
            int maxID = 0;
            foreach (Rezerwacja rezerwacjawBazie in wszystkieRezerwacje)
                if (rezerwacjawBazie.IdRezerwacji > maxID)
                    maxID = rezerwacjawBazie.IdRezerwacji;
            maxID++;

            List<MiejscaRezerwacji> wszystkieMiejsca = repozytoriumMiejscRezerwacji.ZwrocWszystkie();
            int maxID2 = 0;
            foreach (MiejscaRezerwacji miejsceRezerwacji in wszystkieMiejsca)
                if (miejsceRezerwacji.IdMiejscaRezerwacji > maxID2)
                    maxID2 = miejsceRezerwacji.IdMiejscaRezerwacji;
            maxID2++;

            bool error = true;

            // Dodawanie rekordów tutaj

            ADONetSupport ADONet = ADONetSupport.CreateInstance();
            if (ADONet.OpenConnection())
            {
                int iloscBiletow = miejsca.Length;
                string sqlQuery = "";

                sqlQuery = "SET IDENTITY_INSERT Rezerwacjas ON";
                if (ADONet.QueryNoResult(sqlQuery))
                {
                    sqlQuery = "INSERT INTO Rezerwacjas (IdRezerwacji, Imie, Nazwisko, Email, NrTelefonu, IloscBiletow, seans_IdSeansu) VALUES (" + maxID + ", '" + imie + "', '" + nazwisko + "', '" + email + "', '" + telefon + "', " + iloscBiletow + ", " + seansID + ")";
                    if (ADONet.QueryNoResult(sqlQuery))
                    {
                        ADONet.CloseConnection();
                        ADONet.OpenConnection();

                        sqlQuery = "SET IDENTITY_INSERT MiejscaRezerwacjis ON";
                        if (ADONet.QueryNoResult(sqlQuery))
                        {
                            bool fail = false;
                            for (int i = 0; i < miejsca.Length; i++)
                            {
                                // w bazie sa przechowywanie numery miejsc (nie od zera ale od 1)
                                int miejsce = miejsca[i];
                                sqlQuery = "INSERT INTO MiejscaRezerwacjis (IdMiejscaRezerwacji, NumerMiejsca, RezerwacjaId_IdRezerwacji) VALUES (" + maxID2 + ", " + miejsce + ", " + maxID + ") ";

                                if (!ADONet.QueryNoResult(sqlQuery))
                                    fail = true;

                                maxID2++;
                            }

                            if (!fail)
                            {
                                sqlQuery = "SET IDENTITY_INSERT Rezerwacjas OFF";
                                if (ADONet.QueryNoResult(sqlQuery))
                                {
                                    sqlQuery = "SET IDENTITY_INSERT Rezerwacjas OFF";
                                    if (ADONet.QueryNoResult(sqlQuery))
                                        error = false;
                                }
                            }
                        }
                    }
                }

                ADONet.CloseConnection();
            }

            //repozytoriumRezerwacji.Dodaj(rezerwacja);

            string JSONErrors = JsonConvert.SerializeObject(error);
            return Json(JSONErrors, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
