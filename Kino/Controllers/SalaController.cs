﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kino.Models;
using Kino.Services;

namespace Kino.Controllers
{
    public class SalaController : Controller
    {
        private Konteksty db = new Konteksty();

        //
        // GET: /Sala/

        [Autoryzuj(Role = Role.Admin | Role.Menadzer | Role.Planista | Role.Kasjer)]
        public ActionResult Index()
        {
            return View(db.Sala.ToList());
        }

        //
        // GET: /Sala/Details/5

        [Autoryzuj(Role = Role.Admin | Role.Menadzer | Role.Planista | Role.Kasjer)]
        public ActionResult Details(int id = 0)
        {
            Sala sala = db.Sala.Find(id);
            if (sala == null)
            {
                return HttpNotFound();
            }
            return View(sala);
        }

        //
        // GET: /Sala/Create

        [Autoryzuj(Role = Role.Admin | Role.Menadzer)]
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Sala/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Autoryzuj(Role = Role.Admin | Role.Menadzer)]
        public ActionResult Create(Sala sala)
        {
            if (ModelState.IsValid)
            {
                db.Sala.Add(sala);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(sala);
        }

        //
        // GET: /Sala/Edit/5

        [Autoryzuj(Role = Role.Admin | Role.Menadzer)]
        public ActionResult Edit(int id = 0)
        {
            Sala sala = db.Sala.Find(id);
            if (sala == null)
            {
                return HttpNotFound();
            }
            return View(sala);
        }

        //
        // POST: /Sala/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Autoryzuj(Role = Role.Admin | Role.Menadzer)]
        public ActionResult Edit(Sala sala)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sala).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(sala);
        }

        //
        // GET: /Sala/Delete/5

        [Autoryzuj(Role = Role.Admin | Role.Menadzer)]
        public ActionResult Delete(int id = 0)
        {
            Sala sala = db.Sala.Find(id);
            if (sala == null)
            {
                return HttpNotFound();
            }
            return View(sala);
        }

        //
        // POST: /Sala/Delete/5

        [Autoryzuj(Role = Role.Admin | Role.Menadzer)]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Sala sala = db.Sala.Find(id);
            db.Sala.Remove(sala);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}