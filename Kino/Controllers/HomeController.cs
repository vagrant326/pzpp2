﻿using Kino.Services;
using System.Data.Common;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;
namespace Kino.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Strona Główna";
            Models.Konteksty db = new Models.Konteksty();
            ViewBag.Model = db.Aktualnosci.ToList();
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Title = "O nas";
            ViewBag.RejestracjaOtwarta = UsersExist.Answer();
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }
    }
}