﻿using Kino.Models;
using Kino.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace Kino.Controllers
{
    public class KontaktController : Controller
    {

        [Autoryzuj(Role = Role.Admin | Role.Menadzer | Role.Planista | Role.Kasjer)]
        public ActionResult PrzelaczTryb(bool? pracownik)
        {
            ViewBag.Title = "Kontakt";
            string sciezka = Server.MapPath("~/App_Data/kontakt.xml");
            Kontakt kontakt = ObslugaXML.Wczytaj(sciezka);
            if (pracownik == true)
            {
                return View("Admin/Index", kontakt);
            }
            else
            {
                return View("Index", kontakt);
            }
        }

        // GET: Kontakt
        public ActionResult Index()
        {
            ViewBag.Title = "Kontakt";
            string sciezka = Server.MapPath("~/App_Data/kontakt.xml");
            Kontakt kontakt = ObslugaXML.Wczytaj(sciezka);

            if (User.IsInRole(Role.Admin | Role.Menadzer | Role.Planista | Role.Kasjer))
            {
                return View("Admin/Index", kontakt);
            }
            else
            {
                return View(kontakt);
            }
        }

        // GET: /Kontakt/Edit/5
        [Autoryzuj(Role = Role.Admin | Role.Menadzer)]
        public ActionResult Edit(int id = 0)
        {
            string sciezka = Server.MapPath("~/App_Data/kontakt.xml");
            Kontakt kontakt = ObslugaXML.Wczytaj(sciezka);
            return View("Admin/Edit",kontakt);
        }

        //
        // POST: /Kontakt/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Autoryzuj(Role = Role.Admin | Role.Menadzer)]
        public ActionResult Edit(Kontakt kontakt)
        {
            string sciezka = Server.MapPath("~/App_Data/kontakt.xml");
            ObslugaXML.Zapisz(sciezka, kontakt);
            return RedirectToAction("Admin/Index");
        }
    }
}