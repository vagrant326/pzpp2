﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kino.Services;

namespace Kino.Controllers
{
    public class PanelController : Controller
    {
        // GET: Panel
        [Autoryzuj(Role = Role.Admin | Role.Menadzer)]
        public ActionResult Index()
        {
            return View();
        }
    }
}
