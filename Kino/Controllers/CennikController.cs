﻿using Kino.Models;
using Kino.Services;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Kino.Controllers
{
    public class CennikController : Controller
    {
        private Konteksty db = new Konteksty();

        [Autoryzuj(Role = Role.Admin | Role.Menadzer | Role.Planista | Role.Kasjer)]
        public ActionResult PrzelaczTryb(bool? pracownik)
        {
            if (pracownik == true)
            {
                return View("Admin/Index", db.Cennik.ToList());
            }
            else
            {
                return View("Index", db.Cennik.ToList());
            }
        }
        // GET: Cennik
        public ActionResult Index()
        {
            ViewBag.Title = "Cennik";

            if (User.IsInRole(Role.Admin | Role.Menadzer | Role.Planista | Role.Kasjer))
            {
                return View("Admin/Index", db.Cennik.ToList());
            }
            else
            {
                return View(db.Cennik.ToList());
            }

        }

        [Autoryzuj(Role = Role.Admin | Role.Menadzer)]
        public ActionResult Edit(int id = 0)
        {
            Cennik cennik = db.Cennik.Find(id);
            if (cennik == null)
            {
                return HttpNotFound();
            }
            return View("Admin/Edit", cennik);
        }

        //
        // POST: /Cennik/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Autoryzuj(Role = Role.Admin | Role.Menadzer)]
        public ActionResult Edit(Cennik cennik)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cennik).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View("Admin/Edit", cennik);
        }
    }
}
