﻿using System.Web.Mvc;

namespace Kino.Controllers
{
    public class ErrorController : Controller
    {
        // GET: Error
        public ActionResult Unauthorized()
        {
            ViewBag.Title = "Brak uprawnień!";

            return View();
        }
    }
}