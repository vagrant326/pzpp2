﻿using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Kino.Models;
using Kino.Services;

namespace Kino.Controllers
{
    using Repositories;
    public class SeansController : Controller
    {
        private Konteksty db = new Konteksty();

        [Autoryzuj(Role = Role.Admin | Role.Menadzer | Role.Planista | Role.Kasjer)]
        public ActionResult PrzelaczTryb(bool? pracownik)
        {
            if (pracownik == true)
            {
                return View("Admin/Index", db.Seans.ToList());
            }
            else
            {
                ViewBag.Filmy = db.Filmy.AsQueryable<Film>();
                return View("Index",db.Seans.ToList());
            }
        }
        //
        // GET: /Seans/

        public ActionResult Index()
        {
            ViewBag.Title = "Repertuar";
            if (User.IsInRole(Role.Admin | Role.Menadzer | Role.Planista | Role.Kasjer))
            {
                return View("Admin/Index", db.Seans.ToList());
            }
            else
            {
                ViewBag.Filmy = db.Filmy.AsQueryable<Film>();
                return View(db.Seans.ToList());
            }
        }

        //
        // GET: /Seans/Details/5

        [Autoryzuj(Role = Role.Admin | Role.Menadzer | Role.Planista | Role.Kasjer)]
        public ActionResult Details(int id = 0)
        {
            Seans seans = db.Seans.Find(id);
            if (seans == null)
            {
                return HttpNotFound();
            }
            return View(seans);
        }

        //
        // GET: /Seans/Create

        [Autoryzuj(Role = Role.Admin | Role.Menadzer | Role.Planista)]
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Seans/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Autoryzuj(Role = Role.Admin | Role.Menadzer | Role.Planista)]
        public ActionResult Create(Seans seans)
        {
            if (ModelState.IsValid)
            {
                db.Seans.Add(seans);
                db.SaveChanges();
                return RedirectToAction("Admin/Index");
            }

            return View(seans);
        }

        //
        // GET: /Seans/Edit/5

        [Autoryzuj(Role = Role.Admin | Role.Menadzer | Role.Planista)]
        public ActionResult Edit(int id = 0)
        {
            Seans seans = db.Seans.Find(id);
            if (seans == null)
            {
                return HttpNotFound();
            }
            return View(seans);
        }

        //
        // POST: /Seans/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Autoryzuj(Role = Role.Admin | Role.Menadzer | Role.Planista)]
        public ActionResult Edit(Seans seans)
        {
            if (ModelState.IsValid)
            {
                db.Entry(seans).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Admin/Index");
            }
            return View(seans);
        }

        //
        // GET: /Seans/Delete/5

        [Autoryzuj(Role = Role.Admin | Role.Menadzer | Role.Planista)]
        public ActionResult Delete(int id = 0)
        {
            Seans seans = db.Seans.Find(id);
            if (seans == null)
            {
                return HttpNotFound();
            }
            return View(seans);
        }

        //
        // POST: /Seans/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Autoryzuj(Role = Role.Admin | Role.Menadzer | Role.Planista)]
        public ActionResult DeleteConfirmed(int id)
        {
            Seans seans = db.Seans.Find(id);
            db.Seans.Remove(seans);
            db.SaveChanges();
            return RedirectToAction("Admin/Index");
        }

        [HttpPost]
        public JsonResult PobierzWszystkieSeanse()
        {
            return Json(new SqlSeansRepozytorium().ZwrocWszystkie(), JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}