﻿CREATE FUNCTION [dbo].UsersExist ()
RETURNS BIT
AS
BEGIN
	DECLARE	@param bit = 0
	SET @param = CASE WHEN EXISTS 
	(
		SELECT *
		FROM AspNetUsers
	)
	THEN 
			CAST(1 AS BIT)
	ELSE 
			CAST(0 AS BIT)
	END
	RETURN (@param)
END
