USE [aspnet-Kino-20150106073651]
GO

INSERT INTO [dbo].[Salas]
           ([Pojemnosc]
           ,[Niepelnosprawni]
           ,[IloscRzedow])
     VALUES
	(100,0,10),
	(100,5,10),
	(200,10,10),
        (300,10,15),
	(300,10,20),
	(400,10,20),
	(400,20,20),
	(500,20,20),
	(500,20,25),
	(600,20,30)
GO

