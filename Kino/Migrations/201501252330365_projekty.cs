namespace Kino.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class projekty : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Projekts",
                c => new
                    {
                        ProjektId = c.Int(nullable: false, identity: true),
                        Nazwa = c.String(nullable: false),
                        Opis = c.String(nullable: false),
                        AdresGrafiki = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ProjektId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Projekts");
        }
    }
}
