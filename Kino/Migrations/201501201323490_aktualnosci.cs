namespace Kino.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class aktualnosci : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Aktualnoscis",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        sciezkaDoPliku = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Films",
                c => new
                    {
                        IdFilmu = c.Int(nullable: false, identity: true),
                        Tytul = c.String(nullable: false),
                        DataPremiery = c.DateTime(nullable: false),
                        Opis = c.String(nullable: false),
                        LinkYouTube = c.String(),
                        LinkDoZdjecia = c.String(),
                        WiekWidza = c.Int(nullable: false),
                        WersjaFilmu = c.Int(nullable: false),
                        Jezyk = c.String(nullable: false),
                        Dlugosc = c.Int(nullable: false),
                        Gatunek = c.String(nullable: false),
                        OryginalnyTytul = c.String(nullable: false),
                        Rezyser = c.String(nullable: false),
                        Obsada = c.String(nullable: false),
                        Produkcja = c.String(nullable: false),
                        RokProdukcji = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.IdFilmu);
            
            CreateTable(
                "dbo.Rezerwacjas",
                c => new
                    {
                        IdRezerwacji = c.Int(nullable: false, identity: true),
                        Imie = c.String(nullable: false, maxLength: 30),
                        Nazwisko = c.String(nullable: false, maxLength: 40),
                        Email = c.String(nullable: false),
                        NrTelefonu = c.String(nullable: false, maxLength: 11),
                        IloscBiletow = c.Int(nullable: false),
                        seans_IdSeansu = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IdRezerwacji)
                .ForeignKey("dbo.Seans", t => t.seans_IdSeansu, cascadeDelete: true)
                .Index(t => t.seans_IdSeansu);
            
            CreateTable(
                "dbo.Seans",
                c => new
                    {
                        IdSeansu = c.Int(nullable: false, identity: true),
                        data = c.DateTime(nullable: false),
                        film_IdFilmu = c.Int(nullable: false),
                        sala_Numer = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IdSeansu)
                .ForeignKey("dbo.Films", t => t.film_IdFilmu, cascadeDelete: true)
                .ForeignKey("dbo.Salas", t => t.sala_Numer, cascadeDelete: true)
                .Index(t => t.film_IdFilmu)
                .Index(t => t.sala_Numer);
            
            CreateTable(
                "dbo.Salas",
                c => new
                    {
                        Numer = c.Int(nullable: false, identity: true),
                        Pojemnosc = c.Int(nullable: false),
                        Niepelnosprawni = c.Int(nullable: false),
                        IloscRzedow = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Numer);
            
            CreateTable(
                "dbo.UserProfile",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Rezerwacjas", "seans_IdSeansu", "dbo.Seans");
            DropForeignKey("dbo.Seans", "sala_Numer", "dbo.Salas");
            DropForeignKey("dbo.Seans", "film_IdFilmu", "dbo.Films");
            DropIndex("dbo.Seans", new[] { "sala_Numer" });
            DropIndex("dbo.Seans", new[] { "film_IdFilmu" });
            DropIndex("dbo.Rezerwacjas", new[] { "seans_IdSeansu" });
            DropTable("dbo.UserProfile");
            DropTable("dbo.Salas");
            DropTable("dbo.Seans");
            DropTable("dbo.Rezerwacjas");
            DropTable("dbo.Films");
            DropTable("dbo.Aktualnoscis");
        }
    }
}
