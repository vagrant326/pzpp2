namespace Kino.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class miejscaRezerwacji : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MiejscaRezerwacjis",
                c => new
                    {
                        IdMiejscaRezerwacji = c.Int(nullable: false, identity: true),
                        NumerMiejsca = c.Int(nullable: false),
                        RezerwacjaId_IdRezerwacji = c.Int(),
                    })
                .PrimaryKey(t => t.IdMiejscaRezerwacji)
                .ForeignKey("dbo.Rezerwacjas", t => t.RezerwacjaId_IdRezerwacji)
                .Index(t => t.RezerwacjaId_IdRezerwacji);
            
            AddColumn("dbo.Aktualnoscis", "link", c => c.String());
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MiejscaRezerwacjis", "RezerwacjaId_IdRezerwacji", "dbo.Rezerwacjas");
            DropIndex("dbo.MiejscaRezerwacjis", new[] { "RezerwacjaId_IdRezerwacji" });
            DropColumn("dbo.Aktualnoscis", "link");
            DropTable("dbo.MiejscaRezerwacjis");
        }
    }
}
