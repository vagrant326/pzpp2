namespace Kino.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class cennik : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cenniks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RodzajBiletu = c.String(),
                        PnCzw2D = c.Int(nullable: false),
                        PtNd2D = c.Int(nullable: false),
                        PnCzw3D = c.Int(nullable: false),
                        PtNd3D = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Cenniks");
        }
    }
}
