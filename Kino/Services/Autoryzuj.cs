﻿using System;
using System.Linq;
using System.Security.Principal;
using System.Web.Mvc;
using System.Web.Routing;

namespace Kino.Services
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class Autoryzuj : AuthorizeAttribute
    {
        public Role Role { get; set; }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (Role != 0)
                Roles = Role.ToString();

            base.OnAuthorization(filterContext);
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new RedirectToRouteResult(
                        new RouteValueDictionary(
                            new
                            {
                                controller = "Error",
                                action = "Unauthorized"
                            })
                        );
        }
    }
    public static class PrincipalExtensions
    {
        public static bool IsInRole(this IPrincipal user, Role Role)
        {
            var roles = Role.ToString().Split(',').Select(x => x.Trim());
            foreach (var role in roles)
            {
                if (user.IsInRole(role))
                    return true;
            }
            return false;
        }
    }
}