﻿using Kino.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace Kino.Services
{
    public class ObslugaXML
    {
        public static Kontakt Wczytaj(string sciezka)
        {
            Kontakt kontakt = new Kontakt();

            XmlDocument plik = new XmlDocument();
            plik.Load(sciezka);

            XmlNode wezel = plik.SelectSingleNode("kontakt/adres");
            kontakt.Adres = wezel.InnerText;
            wezel = plik.SelectSingleNode("kontakt/email");
            kontakt.Email = wezel.InnerText;
            wezel = plik.SelectSingleNode("kontakt/nrtelefonu");
            kontakt.NrTelefonu = wezel.InnerText;
            wezel = plik.SelectSingleNode("kontakt/fax");
            kontakt.Fax = wezel.InnerText;
            wezel = plik.SelectSingleNode("kontakt/godziny");
            kontakt.GodzinyOtwarcia = wezel.InnerText;
            return kontakt;
        }

        public static void Zapisz(string sciezka, Kontakt kontakt)
        {
            XmlDocument plik = new XmlDocument();
            plik.Load(sciezka);

            XmlNode wezel = plik.SelectSingleNode("kontakt/adres");
            wezel.InnerText = kontakt.Adres;
            wezel = plik.SelectSingleNode("kontakt/email");
            wezel.InnerText = kontakt.Email;
            wezel = plik.SelectSingleNode("kontakt/nrtelefonu");
            wezel.InnerText = kontakt.NrTelefonu;
            wezel = plik.SelectSingleNode("kontakt/fax");
            wezel.InnerText = kontakt.Fax;
            wezel = plik.SelectSingleNode("kontakt/godziny");
            wezel.InnerText = kontakt.GodzinyOtwarcia;

            plik.Save(sciezka);
        }
    }
}