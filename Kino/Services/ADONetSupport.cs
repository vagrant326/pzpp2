﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace Kino.Services
{
    using Models;

    public class ADONetSupport
    {
        SqlConnection sqlConnection = null;
        bool connected = false;


        public static string ErrorMessage = null;


        public static ADONetSupport CreateInstance()
        {
            Konteksty konteksty = new Konteksty();
            return new ADONetSupport(konteksty.Database.Connection.ConnectionString);
        }

        ADONetSupport(string connectionString)
        {
            sqlConnection = new SqlConnection(connectionString);
        }

        public object[][] GetObjectRow(string sqlQuery, string[] cols)
        {
            List<object[]> objects = new List<object[]>();

            if (connected)
            {
                SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnection);
                try
                {
                    SqlDataReader dataReader = sqlCommand.ExecuteReader();
                    while (dataReader.Read())
                    {
                        List<object> rowData = new List<object>();

                        for (int i = 0; i < cols.Length; i++)
                        {
                            rowData.Add(dataReader[cols[i]]);
                        }

                        objects.Add(rowData.ToArray());
                    }
                }
                catch (Exception e)
                {
                    ErrorMessage = e.Message;
                    return null;
                }
            }

            return objects.ToArray();
        }

        public void CloseConnection()
        {
            if (!connected)
                return;

            sqlConnection.Close();
        }

        public bool QueryNoResult(string sqlQuery)
        {
            if (connected)
            {
                SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnection);
                try
                {
                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    ErrorMessage = e.Message;
                    return false;
                }

                return true;
            }

            return false;
        }

        public bool IsConnected()
        {
            return connected;
        }

        public bool OpenConnection()
        {
            try
            {
                sqlConnection.Open();
            }
            catch (Exception e)
            {
                ErrorMessage = e.Message;
                connected = false;
                return false;
            }

            connected = true;
            return true;
        }
    }
}