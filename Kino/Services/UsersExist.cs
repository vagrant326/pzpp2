﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Kino.Services
{
    public class UsersExist
    {
        public static bool Answer()
        {
            Models.Konteksty db = new Models.Konteksty();
            Database database = db.Database;
            bool odpowiedz;
            using (DbCommand dbCommand = new System.Data.SqlClient.SqlCommand("SELECT dbo.UsersExist()", (SqlConnection)db.Database.Connection))
            {
                db.Database.Connection.Open();
                odpowiedz = !(bool)dbCommand.ExecuteScalar(); //not exists
                db.Database.Connection.Close();
            }
            return odpowiedz;
        }
    }
}