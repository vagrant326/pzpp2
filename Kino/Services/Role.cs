﻿using System;

namespace Kino.Services
{
    //public static class Role
    //{
    //    public static string Admin = "Admin";
    //    public static string Menadzer = "Menadzer" ;
    //    public static string Planista = "Planista" ;
    //    public static string Kasjer = "Kasjer" ;
    //}

    [Flags]
    public enum Role
    {
        Admin = 1,
        Menadzer = 2,
        Planista = 4,
        Kasjer = 8
    }
}