﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kino.Models;

namespace Kino.Services
{
    public class TimeComparer:IEqualityComparer<Seans>
    {
        public bool Equals(Seans x, Seans y)
        {
            if (x.data==y.data)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public int GetHashCode(Seans seans)
        {
            return seans.data.GetHashCode();
        }
    }
}