﻿// Plik wspólny dla wszystkich podstron
// Tutaj prosze umieszczać funkcje które 
// będą używane na wielu podstronach

function mvcDateTimeToJSDate(mvcDateTime) {
    return new Date(parseInt(mvcDateTime.replace("/Date(", "").replace(")/", ""), 10));
}