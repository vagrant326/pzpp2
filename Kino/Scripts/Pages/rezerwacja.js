﻿window.onerror = function (a, b, c) {
    alert(a+"\n"+b+"\n"+c)
}

function dateInRange(currentDate, dateA, dateB) {
    return currentDate >= dateA && currentDate <= dateB;
}

/* http://stackoverflow.com/questions/1197928/how-to-add-30-minutes-to-a-javascript-date-object */
function getSeanseDuration(startTime, movieDuration) {
    return [startTime = mvcDateTimeToJSDate(startTime), new Date(startTime.getTime() + movieDuration * 60000)];
}

function formatHMS(date) {
    return (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ":" + (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ":" + (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
}

function formatDate(date) {
    return (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()) + "-" + ((date.getMonth() + 1) < 10 ? '0' + (date.getMonth() + 1) : (date.getMonth() + 1)) + "-" + date.getFullYear();
}

function daysEqual(dateA, dateB) {
    return (dateA.getDate() == dateB.getDate() && dateA.getMonth() == dateB.getMonth() && dateA.getFullYear() == dateB.getFullYear());
}

/* http://stackoverflow.com/questions/17898008/jquery-datepicker-beforeshowday-after-initializing */
function updateScreenings() {
    $jQ('nbs-calendar div').datepicker('option', 'beforeShowDay', updateDatePickerCallback);
}

function isEmpty(text)
{
    return (text.replace(new RegExp(' ', 'g'), '').replace(new RegExp('\t', 'g'), '').replace(new RegExp('\n', 'g'), '')).length == 0;
}

function reserverTickets(form) {
    
    //if (window.confirm('Czy jesteś pewien że wprowadzone dane są prawidłowe?')) { } else { return false }

    if (!movieSelected)
    {
        alert("Zanzacz film");
        return false;
    }

    if (selectedSeanse == null) {
        alert("Wybierz godziny seansu");
        return false;
    }

    if (positionsReserved.length == 0) {
        alert("Wybierz zaznacz które miejsca w sali chcesz zarezerwować");
        return false;
    }
    
    for (var i = 0; i < form.elements.length; i++) {
        if (form.elements[i].type == 'text') {
            if (isEmpty(form.elements[i].value)){
                alert("Wypełnij puste pola");
                return false;
            }
        }
    }
    
    var options = {};
    options.url = "/Rezerwacja/Rezerwuj";
    options.type = 'POST';
    options.dataType = 'json';
    options.cache = false;
    options.data = {
        Imie: form.first_name.value,
        Nazwisko: form.last_name.value,
        Email: form.email.value,
        Telefon: form.phone_number.value,
        Seans: selectedSeanse.IdSeansu,
        Miejsca: positionsReserved.join(".")
    }
    
    options.success = function (error) {
        if (error === 'false') {
            alert("Rezeracja przebiegła pomyślnie");
            window.location.href = indexURL;
            return true;
        }
    };

    options.error = function () { alert("Wystąpił błąd, spróbuj ponownie"); location.reload(); };
    $jQ.ajax(options);
    return true;
}

var pRes = null;

function getReservedPositions(SeansID) {
    var options = {};
    options.url = "/Rezerwacja/PobierzZarezerwowaneMiejsca";
    options.dataType = "json";
    options.type = 'POST';
    options.async = false;
    options.data = {
        seansID: SeansID
    };

    options.success = function (results) {
        pRes = results;
    };

    options.error = function (e) { alert("Wystąpił błąd przy rezerwacji, spróbuj ponownie\n"+e);  };
    $jQ.ajax(options);
}

function selectSeanseTime(seansesSelect) {
    if (seansesSelect.selectedIndex >= 0) {
        var seanseID = seansesSelect.options[seansesSelect.selectedIndex].value;
        for (var i = 0; i < screeningsData.length; i++) {
            if (screeningsData[i].IdSeansu == seanseID) {
                selectedSeanse = screeningsData[i];
                clearCanvas();
                pRes = null;
                var reservedPlaces = getReservedPositions(seanseID);
                while (pRes == null);
                drawRoom(screeningsData[i].sala.Pojemnosc, pRes, screeningsData[i].sala.Niepelnosprawni, screeningsData[i].sala.IloscRzedow);
            }
        }
    }
}

function getCanvasPositionStyle(type) {
    switch (type) {
        case 'available':
            return $jQ('#nbs-positions-canvas').createGradient({
                x1: 0, y1: 0,
                x2: 30, y2: 30,
                c1: '#090', c2: '#090'
            });
        case 'inaccessible':
            return $jQ('#nbs-positions-canvas').createGradient({
                x1: 0, y1: 0,
                x2: 10, y2: 10,
                c1: '#600', c2: '#600'
            });
        case 'now-reserved':
            return $jQ('#nbs-positions-canvas').createGradient({
                x1: 0, y1: 0,
                x2: 15, y2: 15,
                c1: '#c00', c2: '#c00'
            });
        case 'disabled':
            return $jQ('#nbs-positions-canvas').createGradient({
                x1: 0, y1: 0,
                x2: 15, y2: 15,
                c1: '#009', c2: '#009'
            });
    }
}

function initCanvas() {
    $jQ('#nbs-positions-canvas').scaleCanvas({ scale: 1.0 });
    $jQ('#nbs-positions-canvas').setLayer({
        type: 'rectangle',
        fillStyle: '#009',
        x: 0, y: 0,
        width: roomConfig.canvasSize[0], height: roomConfig.canvasSize[1]
    }).drawLayer();
}

function clearCanvas() {
    var canvasWidth = roomConfig.canvasSize[0];
    var canvasHeight = roomConfig.canvasSize[1];

    $jQ('#nbs-positions-canvas').drawRect({
        layer: true,
        fillStyle: '#000',
        width: canvasWidth * 2,
        height: canvasHeight * 2,
        x: 0,
        y: 0
    });
}

function drawBaseCanvas() {
    var canvasWidth = roomConfig.canvasSize[0];
    var canvasHeight = roomConfig.canvasSize[1];

    clearCanvas();

    $jQ('#nbs-positions-canvas').drawText({
        layer: true,
        fillStyle: '#fff',
        strokeStyle: '#fff',
        strokeWidth: 0,
        x: canvasWidth/2, y: canvasHeight/2,
        fontSize: 40,
        fontFamily: 'Verdana, sans-serif',
        text: "Wybierz godziny seansu aby wybrać miejsce"
    });
}

function updatePositions() {
    tmpPos = [];
    for (var i = 0; i < positionsReserved.length; i++)
        tmpPos.push(positionsReserved[i] + 1);

    $jQ('#nbs-selected-positions-label').html(tmpPos.join(', '));
}

var timerHandle = null;

function positionPlaceClick(position) {

    var positionID = position.ID;
    if (positionID < 0 || timerHandle != null) return; // -1 pojawia sie dla zarezerwowanych pozycji

    $jQ('#nbs-positions-canvas').css('cursor', 'wait');

    timerHandle = setTimeout(function () {

        var alreadyReserved = false;
        for (var i = 0; i < positionsReserved.length; i++) {
            if (positionsReserved[i] == positionID)
                alreadyReserved = true;
        }

        setTimeout(function () {
            if (alreadyReserved) {
                drawPosition(positionID, originalPositionsTypes[positionID], position.x, position.y);
                newPos = []; // .splice(index, 1) nie dziala
                for (var i = 0; i < positionsReserved.length; i++)
                    if (positionsReserved[i] != positionID)
                        newPos.push(positionsReserved[i]);
                positionsReserved = newPos;
            } else {
                drawPosition(positionID, 'now-reserved', position.x, position.y);
                positionsReserved.push(positionID);
            }
            $jQ('#nbs-positions-canvas').css('cursor', 'default');
            updatePositions();
            timerHandle = null;
        }, 200);

    }, 100);
}

function positionBoxOverPos() {
    $jQ('#nbs-positions-canvas').css('cursor', 'pointer');
}

function positionBoxOutPos() {
    $jQ('#nbs-positions-canvas').css('cursor', 'default');
}

function drawPosition(posID, type, x, y) {

    $jQ('#nbs-positions-canvas').drawRect({
        layer: true,
        fillStyle: getCanvasPositionStyle(type),
        width: roomConfig.positionSize[0],
        height: roomConfig.positionSize[1],
        cornerRadius: 7,
        x: x,
        y: y,
        shadowColor: '#000',
        shadowBlur: 5,
        shadowX: 2, shadowY: 2,
        ID: posID,
        click: positionPlaceClick
    });

    $jQ('#nbs-positions-canvas').drawText({
        layer: true,
        fillStyle: '#fff',
        strokeStyle: '#fff',
        strokeWidth: 0,
        x: x, y: y,
        fontSize: 12,
        fontFamily: 'Verdana, sans-serif',
        text: (posID + 1),
    });
}

var originalPositionsTypes = [];

function drawRoom(positions, reserverdPos, availPosForDisabled, rowsCount) {

    var canvasWidth = roomConfig.canvasSize[0];
    var canvasHeight = roomConfig.canvasSize[1];

    var positionDimension = [roomConfig.positionSize[0], roomConfig.positionSize[1]]; // [W, H]

    var rows = rowsCount;
    var cols = ((positions % rowsCount) == 0) ? (positions / rowsCount) : (Math.ceil(positions / rowsCount));

    var offsetX = roomConfig.offsetX, offsetY = roomConfig.topOffsetY;
    var bottomOffset = roomConfig.bottomOffsetY;

    var ox2 = canvasWidth - (2 * offsetX);
    var totalPaddingX = (ox2 - (cols * positionDimension[0]));
    var posPaddingX = Math.round(totalPaddingX / (cols - 1));

    var oy2 = canvasHeight - (bottomOffset + offsetY);
    var totalPaddingY = (oy2 - (rows * positionDimension[1]));
    var posPaddingY = Math.round(totalPaddingY / (rows - 1));

    clearCanvas();

    for (var i = 0; i < rows; i++) {
        for (var j = 0; j < cols; j++) {
            var posStyle;
            var posX = offsetX + ((j * positionDimension[0]) + (posPaddingX * j));
            var posY = offsetY + ((i * positionDimension[1]) + (posPaddingY * i));

            var posID = (i * cols) + j;
            var disablePos = false;

            if (posID >= positions) break; // Nie wypisuj wszystkich miejsc
            posStyle = getCanvasPositionStyle('available');
            originalPositionsTypes[posID] = 'available';
            
            if (posID < availPosForDisabled) {
                posStyle = getCanvasPositionStyle('disabled');
                originalPositionsTypes[posID] = 'disabled';
            }

            for (var k = 0; k < reserverdPos.length; k++) {
                if (reserverdPos[k] == posID+1) {
                    disablePos = true;
                    posStyle = getCanvasPositionStyle('inaccessible');
                    originalPositionsTypes[posID] = 'inaccessible';
                }
            }

            $jQ('#nbs-positions-canvas').drawRect({
                layer: true,
                fillStyle: posStyle,
                width: positionDimension[0],
                height: positionDimension[1],
                cornerRadius: 7,
                x: posX,
                y: posY,
                shadowColor: '#000',
                shadowBlur: 5,
                shadowX: 2, shadowY: 2,
                ID: disablePos ? -1 : posID,
                click: positionPlaceClick
            });

            $jQ('#nbs-positions-canvas').drawText({
                layer: true,
                fillStyle: '#fff',
                strokeStyle: '#fff',
                strokeWidth: 0,
                x: posX, y: posY,
                fontSize: 12,
                fontFamily: 'Verdana, sans-serif',
                text: (posID + 1)
            });
        }
    }

    var offset2y = 30;
    var offset2x = offsetX + positionDimension[0] + 5;
    var labelPosY = oy2 + offsetY + offset2y;
    var labelPosX = offsetX;

    $jQ('#nbs-positions-canvas').drawRect({
        layer: true,
        fillStyle: getCanvasPositionStyle('disabled'),
        width: positionDimension[0],
        height: positionDimension[1],
        cornerRadius: 7,
        y: labelPosY,
        x: labelPosX,
        shadowColor: '#000',
        shadowBlur: 5,
        shadowX: 2, shadowY: 2
    });
    $jQ('#nbs-positions-canvas').drawText({
        layer: true,
        fillStyle: '#fff',
        strokeStyle: '#fff',
        strokeWidth: 0,
        y: labelPosY,
        x: labelPosX + positionDimension[0] + 110,
        fontSize: 12,
        fontFamily: 'Verdana, sans-serif',
        text: " - miejsca dla osób niepełnosprawnych"
    });



    $jQ('#nbs-positions-canvas').drawRect({
        layer: true,
        fillStyle: getCanvasPositionStyle('available'),
        width: positionDimension[0],
        height: positionDimension[1],
        cornerRadius: 7,
        y: labelPosY + (offset2y + positionDimension[1]),
        x: labelPosX,
        shadowColor: '#000',
        shadowBlur: 5,
        shadowX: 2, shadowY: 2
    });
    $jQ('#nbs-positions-canvas').drawText({
        layer: true,
        fillStyle: '#fff',
        strokeStyle: '#fff',
        strokeWidth: 0,
        y: labelPosY + (offset2y + positionDimension[1]),
        x: labelPosX + positionDimension[0] + 50,
        fontSize: 12,
        fontFamily: 'Verdana, sans-serif',
        text: " - dostępne miejsca"
    });



    $jQ('#nbs-positions-canvas').drawRect({
        layer: true,
        fillStyle: getCanvasPositionStyle('inaccessible'),
        width: positionDimension[0],
        height: positionDimension[1],
        cornerRadius: 7,
        y: labelPosY + ((offset2y + positionDimension[1]) * 2),
        x: labelPosX,
        shadowColor: '#000',
        shadowBlur: 5,
        shadowX: 2, shadowY: 2
    });
    $jQ('#nbs-positions-canvas').drawText({
        layer: true,
        fillStyle: '#fff',
        strokeStyle: '#fff',
        strokeWidth: 0,
        y: labelPosY + ((offset2y + positionDimension[1]) * 2),
        x: labelPosX + positionDimension[0] + 40,
        fontSize: 12,
        fontFamily: 'Verdana, sans-serif',
        text: " - zajęte miejsca"
    });

    $jQ('#nbs-positions-canvas').drawText({
        layer: true,
        fillStyle: '#fff',
        strokeStyle: '#fff',
        strokeWidth: 0,
        x: canvasWidth/2,
        y: offsetY-50,
        fontSize: 12,
        fontFamily: 'Verdana, sans-serif',
        text: "ekran projekcji"
    });

    var lineX2 = offsetX + positionDimension[0] + ((cols - 1) * (posPaddingX + positionDimension[0]));
    var lineX1 = offsetX - 30;

    $jQ('#nbs-positions-canvas').drawLine({
        layer: true,
        rounded: true,
        fillStyle: '#333',
        strokeStyle: '#333',
        strokeWidth: 5,
        x1: lineX1, y1: offsetY - 30,
        x2: lineX2, y2: offsetY - 30
    });
}

function addSeanseEndTime(screenings) {
    for (var i = 0; i < screenings.length; i++) { screenings[i]['CzasTrwania'] = getSeanseDuration(screenings[i].data, screenings[i].film.Dlugosc); }
    return screenings;
}

function setDatePickerDayDateToCurrent(datepickerDate) {
    var nowDate = new Date();
        datepickerDate.setHours(nowDate.getHours());
        datepickerDate.setMinutes(nowDate.getMinutes());
        datepickerDate.setSeconds(nowDate.getSeconds());
    return datepickerDate;
}

function highlightTodayDate() {
    if (toggleToday) {
        if ($jQ('#nbs-today-cell') != null)
            $jQ('#nbs-today-cell').attr('id', '');

        var todayDay = $jQ('td.today');
        todayDay.removeClass('today');
        todayDay.attr('id', '#nbs-today-cell');
        todayDay.addClass(toggleToday);

    } else {
        if ($jQ('#nbs-today-cell') != null)
            $jQ('#nbs-today-cell').attr('id', '');

        var todayDay = $jQ('td.today');
        todayDay.attr('id', '#nbs-today-cell');
    }
}

function isMovieAvailable(seanses, currentDate) {
    for (var i = 0; i < seanses.length; i++)
        if (seanses[i].CzasTrwania[0] > currentDate)
            return true;
    return false;
}

function showMovieDetails(movieID) {
    var selectedMovieData;

    for (var i = 0; i < screeningsData.length; i++) {
        if (screeningsData[i].film.IdFilmu == movieID)
            selectedMovieData = screeningsData[i].film;
    }

    if (!selectedMovieData)
        return;

    $jQ('#nbs-movie-detail-title').html(selectedMovieData.Tytul);
    $jQ('#nbs-movie-detail-premiere-date').html(formatDate(mvcDateTimeToJSDate(selectedMovieData.DataPremiery)));
    $jQ('#nbs-movie-detail-description').html(selectedMovieData.Opis);
    $jQ('#nbs-movie-detail-spectator-age').html(selectedMovieData.WiekWidza);
    $jQ('#nbs-movie-detail-language').html(selectedMovieData.Jezyk);
    $jQ('#nbs-movie-detail-length').html(selectedMovieData.Dlugosc+' minut');
    $jQ('#nbs-movie-detail-director').html(selectedMovieData.Rezyser);
    $jQ('#nbs-movie-detail-cast').html(selectedMovieData.Obsada);
    $jQ('#nbs-movie-detail-production').html(selectedMovieData.Produkcja);
    $jQ('#nbs-movie-detail-production-year').html(selectedMovieData.RokProdukcji);
    $jQ('#nbs-movie-detail-genre').html(selectedMovieData.Gatunek.toLowerCase());

    $jQ('#nbs-movie-detail-cover img').remove();
    $jQ('#nbs-movie-detail-cover').append('<img src="' + selectedMovieData.LinkDoZdjecia + '" style="width: 200px; height: 280px;">');
}

function selectMovie(movieID) {
    showMovieDetails(movieID);
    movieSelected = true;

    // Ładuj godziny seansów
    $jQ('#nbs-seanse-time-select').empty();

    for (var i = 0; i < screeningsData.length; i++) {
        if (screeningsData[i].film.IdFilmu == movieID) {
            var seanseTime = screeningsData[i].CzasTrwania;
            movieTitle = screeningsData[i].film.Tytul;
            document.getElementById("nbs-seanse-time-select").options.add(new Option(formatHMS(seanseTime[0])+" - "+formatHMS(seanseTime[1])+", sala nr."+screeningsData[i].sala.Numer, screeningsData[i].IdSeansu));
        }
    }
    $jQ('#nbs-movie-title').html(movieTitle);
    drawBaseCanvas();
    positionsReserved = [];
    positionsReserved.length = 0;
    updatePositions();
}

function clearMovieDetails() {
    selectedSeanse = null;
    if (movieSelected)
        return;
    $jQ('.nbs-movie-detail-info').html('- - -');
    $jQ('#nbs-movie-title').html('');
    $jQ('#nbs-movie-detail-cover img').remove();
}

function clearSeanseSelectTimes() {
    $jQ('#nbs-seanse-time-select').empty();
    document.getElementById("nbs-seanse-time-select").options.add(new Option(" - Wybierz film - ", null));
    document.getElementById("nbs-seanse-time-select").options[0].disabled = true;
}

function loadMoviesInSelectedDay(selectedDate) {
    
    var currentDate = new Date();
    var seansesInDay = [];

    $jQ('.nbs-movie-position').remove();

    for (var i = 0; i < screeningsData.length; i++) {

        if (daysEqual(screeningsData[i].CzasTrwania[0], selectedDate)) {

            var foundSeanse = false;

            for (var j = 0; j < seansesInDay.length; j++)
                for (var k = 0; k < seansesInDay[j].length; k++)
                    if (seansesInDay[j][k].IdSeansu == screeningsData[i].IdSeansu)
                        foundSeanse = true;

            if (!foundSeanse)
            {
                var foundMovie = false;

                for (var j = 0; j < seansesInDay.length; j++) {
                    for (var k = 0; k < seansesInDay[j].length; k++) {
                        if (seansesInDay[j][k].film.IdFilmu == screeningsData[i].film.IdFilmu)
                            foundMovie = true;
                    }
                }

                if (!foundMovie) {
                    seansesInDay.push( [ screeningsData[i] ] );
                } else {
                    for (var j = 0; j < seansesInDay.length; j++) {
                        var foundInnerMovie = false;

                        for (var k = 0; k < seansesInDay[j].length; k++) {
                            if (seansesInDay[j][k].film.IdFilmu == screeningsData[i].film.IdFilmu)
                                foundInnerMovie = true;
                        }

                        if (foundInnerMovie) {
                            var foundInnerSeanse = false;

                            for (var k = 0; k < seansesInDay[j].length; k++)
                                if (seansesInDay[j][k].IdSeansu == screeningsData[i].IdSeansu)
                                    foundInnerSeanse = true;
                            if (!foundInnerSeanse)
                                seansesInDay[j].push(screeningsData[i]);
                        }
                    }
                }
            }
        }
    }

    for (var i = 0; i < seansesInDay.length; i++) {

        var movieAvailable = (isMovieAvailable(seansesInDay[i], currentDate) ? 'available' : 'inaccessible');
        var tooltip = (isMovieAvailable(seansesInDay[i], currentDate) ? 'Kliknij aby wybrać' : 'Ten film jest niedostępny');
        //onclick='selectMovie(" + seansesInDay[i][0].film.IdFilmu + ");'
        //onmouseover='showMovieDetails(' + seansesInDay[i][0].film.IdFilmu + ');'
        var movieHTML = "<img onmouseover='" + (isMovieAvailable(seansesInDay[i], currentDate) ? '' : 'showMovieDetails(' + seansesInDay[i][0].film.IdFilmu + ');') + "' onclick='" + (isMovieAvailable(seansesInDay[i], currentDate) ? 'selectMovie(' + seansesInDay[i][0].film.IdFilmu + ');' : '') + "' onmouseout='clearMovieDetails();' src='" + seansesInDay[i][0].film.LinkDoZdjecia + "' title='" + tooltip + "' class='nbs-movie-position nbs-movie-" + movieAvailable + " nbs-radius' />";
        $jQ('#nbs-movies').append(movieHTML);
    }

    positionsReserved = [];
    positionsReserved.length = 0;
    updatePositions();
}

/* http://stackoverflow.com/questions/22514772/highlight-certain-dates-on-bootstrap-datepicker */
var loadEventsCallback = updateDatePickerCallback = function (date) {

    var seansesInCurrentDayExists = false;
    var foundSeanses = false;
    var seansesInCurrentDay = [];

    // Ustaw bieżącą godzinę w dzisiejszym dniu
    date = (daysEqual(date, new Date()) ? setDatePickerDayDateToCurrent(date) : date);

    for (var i = 0; i < screeningsData.length; i++) {
        
        // Czy są jakikolwiek seanse w dniu z kalendarza 'd'
        if (daysEqual(date, screeningsData[i].CzasTrwania[0])) {
            seansesInCurrentDayExists = true;
            if (!daysEqual(date, new Date())) {
                return { classes: 'highlight-' + (date > new Date() ? 'available' : 'inaccessible') };
            } else {
                if (date < screeningsData[i].CzasTrwania[0])
                    foundSeanses = true;
            }
        }
    }

    // Jeżeli są jakieś seanse w dzisiejszym dniu i te seanse będą jeszcze wyświetlane to zaznacz na zielono
    if (seansesInCurrentDayExists) {
        toggleToday = 'highlight-today-' + (foundSeanses ? 'available' : 'inaccessible');
    }
}