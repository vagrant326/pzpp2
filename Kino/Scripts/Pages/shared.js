﻿// Plik dla podstrony warstwy _Layout.cshtml

// Wymagane skrypty:
// prototype includowany jako pierwszy w HEAD
// jQuery includowany jako drugi w HEAD

// Instancja jQuery (jak zmieniasz zmien tez w _Layout.cshtml)
var $jQ, prev$handle = $, next$Handle;

if ($ && jQuery) {
    // Od tej linijki $ jest zarezerwowany dla prototype'a
    $jQ = jQuery.noConflict();
}

next$Handle = $;

// Sprawdzenie czy nowa instancja działa: true - tak, false - nie
//alert(typeof $jQ === 'function' && prev$handle != next$Handle);