﻿// Tablica przechowujaca wszystkie filmy
var filmy = [];

function pobierzLinkZwiastunu(link) {
    // https://www.youtube.com/watch?v=<FILM_ID> -> http://www.youtube.com/v/<FILM_ID>

    spl = link.split('=');
    if (spl.length != 2) return;

    return 'http://www.youtube.com/v/' + spl[1];
}

function schowajPopup(e) {
    if (e.target.getAttribute('id') == 'nbs-overlay') {

        new Effect.Puff('nbs-overlay-popup', { duration: 1.0 });
        setTimeout(function () {
            $jQ('#nbs-overlay').css('display', 'none');
            $jQ('#nbs-overlay-popup').css('display', 'none');
            //$jQ('#nbs-overlay-popup').css('top', '-800px');
            //$jQ('#nbs-overlay-popup').css('position', 'absolute');
        }, 900);
    }
}

function pokazFilm(e) {
    var filmID = e.target.getAttribute('id')
    for (var i = 0; i < filmy.length; i++) {
        if (filmy[i].IdFilmu == filmID) {

            // Laduj dane filmu
            $jQ('#nbs-movie-name').html(filmy[i].Tytul);

            // http://stackoverflow.com/questions/726334/asp-net-mvc-jsonresult-date-format
            var dataPremiery = mvcDateTimeToJSDate(filmy[i].DataPremiery);

            var gatunki = filmy[i].Gatunek.replace(new RegExp(' ', 'g'), '').toLowerCase().split(',');
            var sciezkaDoObrazkaGatunku = '/Content/images/gatunek/';

            genresDiv = document.getElementById("nbs-movie-genre");
            $jQ('.nbs-movie-genre-image').remove();
            
            for (var j = 0; j < gatunki.length; j++) {
                var obrazGatunku = document.createElement("img");
                obrazGatunku.setAttribute('src', sciezkaDoObrazkaGatunku + gatunki[j] + '.jpg');
                obrazGatunku.className = 'nbs-movie-genre-image';
                obrazGatunku.style.margin = '5px';
                obrazGatunku.style.height = '220px';
                obrazGatunku.style.width = '150px';
                obrazGatunku.style.display = 'inline-block';
                obrazGatunku.style.cursor = 'default';
                genresDiv.appendChild(obrazGatunku);
            }

            $jQ('#nbs-movie-premiere-date').html(dataPremiery.getDate() + "-" + (dataPremiery.getMonth() + 1) + "-" + dataPremiery.getFullYear());
            $jQ('#nbs-movie-description').html(filmy[i].Opis);
            $jQ('#nbs-movie-spectator-age').html(filmy[i].WiekWidza);
            $jQ('#nbs-movie-language').html(filmy[i].Jezyk);
            $jQ('#nbs-movie-length').html(filmy[i].Dlugosc);
            $jQ('#nbs-movie-original-title').html(filmy[i].OryginalnyTytul);
            $jQ('#nbs-movie-director').html(filmy[i].Rezyser);
            $jQ('#nbs-movie-casting').html(filmy[i].Obsada);
            $jQ('#nbs-movie-production').html(filmy[i].Produkcja);
            $jQ('#nbs-movie-production-year').html(filmy[i].RokProdukcji);

            // Okladka
            $jQ('#nbs-popup-cover').css('background-image', 'url("' + filmy[i].LinkDoZdjecia + '")');

            // Zwiastun powinien sie przeladowywac ale nie wiem czy na wszystkich przegladarkach bedzie dzialac
            $jQ('#nbs-trailer-player-config-filename').attr('value', pobierzLinkZwiastunu(filmy[i].LinkYouTube));
            document['nbs-trailer-player'].setAttribute('src', pobierzLinkZwiastunu(filmy[i].LinkYouTube));

            // Pokaz popup
            $jQ('#nbs-overlay').css('display', 'table');

            $jQ('#nbs-overlay-popup').css('display', 'table-cell');
            //var left = $jQ('#nbs-overlay-popup').css('left');
            //$jQ('#nbs-overlay-popup').css('top', '-800px');
            //new Effect.Move('nbs-overlay-popup', { x: left, y: Math.round((innerHeight/2) - 450), mode: 'absolute', duration: 0.7 });
        }
    }
}

function ladujFilmy(filmyArr, contentDIV, rowDivClassName) {

    var photosInRow = 3; // ilosc zdjec w jednym rzedzie
    var rowDivsLength = Math.round(filmyArr.length / photosInRow);
    var isMod = (filmyArr.length % photosInRow) != 0;

    for (var i = 0; i < (rowDivsLength + (isMod ? 1 : 0)) ; ++i) {
        rowDiv = document.createElement("div");
        rowDiv.className = rowDivClassName;
        var startIndex = photosInRow * i;
        var endIndex = startIndex + ((i == rowDivsLength) ? (Math.round(filmyArr.length % photosInRow) - 1) : (photosInRow - 1));

        for (var j = startIndex; j <= endIndex; j++) {
            img = document.createElement("img");
            img.setAttribute('id', filmyArr[j].IdFilmu);
            img.onclick = pokazFilm;
            img.setAttribute('src', filmyArr[j].LinkDoZdjecia); // mozna uzyc bezposrednio tez img.src = ... ale nie wiem czym to sie rozni i co jest lepsze
            rowDiv.appendChild(img);
        }

        contentDIV.appendChild(rowDiv);
    }
}