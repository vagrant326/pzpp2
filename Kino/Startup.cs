﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Kino.Startup))]
namespace Kino
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            //Database.SetInitializer(new DropCreateDatabaseAlways<Konteksty>()); //TODO usunąć po fazie produkcji
        }
    }
}
