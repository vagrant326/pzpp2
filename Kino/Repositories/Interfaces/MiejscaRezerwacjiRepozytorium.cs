﻿using System.Collections.Generic;
using System.Linq;

namespace Kino.Repositories.Interfaces
{
    using Models;

    public abstract class MiejscaRezerwacjiRepozytorium : IRepozytorium<MiejscaRezerwacji>
    {
        public List<MiejscaRezerwacji> ZwrocWszystkie()
        {
            // patrz SeansRepozytorium.cs
            using (var kontekstBazyDanych = new Konteksty())
            {
                kontekstBazyDanych.Configuration.LazyLoadingEnabled = false;
                return (from miejsce in kontekstBazyDanych.Miejsca select miejsce).ToList();
            }
        }

        public List<string> GetErrorMessages()
        {
            return new List<string>();
        }

        public MiejscaRezerwacji ZwrocZaPomocaId(int id)
        {
            using (var kontekstBazyDanych = new Konteksty())
                return kontekstBazyDanych.Miejsca.Where(m => m.IdMiejscaRezerwacji == id).FirstOrDefault();
        }

        public int Policz()
        {
            using (var kontekstBazyDanych = new Konteksty())
                return (from miejsca in kontekstBazyDanych.Miejsca select miejsca).Count();
        }

        public void Dodaj(MiejscaRezerwacji byt)
        {
            using (var kontekstBazyDanych = new Konteksty())
            {
                kontekstBazyDanych.Entry<MiejscaRezerwacji>(byt);
                kontekstBazyDanych.SaveChanges();
            }
        }

        public void Aktualizuj(MiejscaRezerwacji byt)
        {
            using (var kontekstBazyDanych = new Konteksty())
            {
                MiejscaRezerwacji miejsce = kontekstBazyDanych.Miejsca.Where(m => m.IdMiejscaRezerwacji == byt.IdMiejscaRezerwacji).FirstOrDefault();
                if (miejsce != null)
                {
                    kontekstBazyDanych.Entry(miejsce).CurrentValues.SetValues(byt);
                    kontekstBazyDanych.SaveChanges();
                }
            }
        }

        public void UsunZaPomocaId(int id)
        {
            using (var kontekstBazyDanych = new Konteksty())
            {
                var miejsce = kontekstBazyDanych.Miejsca.Where(m => m.IdMiejscaRezerwacji == id).FirstOrDefault();
                if (miejsce != null)
                {
                    kontekstBazyDanych.Miejsca.Remove(miejsce);
                    kontekstBazyDanych.SaveChanges();
                }
            }
        }

        public void UsunWszystkie()
        {
            using (var kontekstBazyDanych = new Konteksty())
            {
                List<MiejscaRezerwacji> wszystkieMiejsca = ZwrocWszystkie();
                foreach (MiejscaRezerwacji miejsce in wszystkieMiejsca)
                    kontekstBazyDanych.Miejsca.Remove(miejsce);

                kontekstBazyDanych.SaveChanges();
            }
        }
    }
}
