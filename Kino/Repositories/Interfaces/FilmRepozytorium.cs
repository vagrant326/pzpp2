﻿using System.Collections.Generic;
using System.Linq;

namespace Kino.Repositories.Interfaces
{
    using Models;

    public abstract class FilmRepozytorium : IRepozytorium<Film>
    {
        public List<Film> ZwrocWszystkie()
        {
            // patrz SeansRepozytorium.cs
            using (var kontekstBazyDanych = new Konteksty())
            {
                kontekstBazyDanych.Configuration.LazyLoadingEnabled = false;
                return (from film in kontekstBazyDanych.Filmy select film).ToList();
            }
        }

        public List<string> GetErrorMessages()
        {
            return new List<string>();
        }

        public Film ZwrocZaPomocaId(int id)
        {
            using (var kontekstBazyDanych = new Konteksty())
                return kontekstBazyDanych.Filmy.Where(m => m.IdFilmu == id).FirstOrDefault();
        }

        public int Policz()
        {
            using (var kontekstBazyDanych = new Konteksty())
                return (from film in kontekstBazyDanych.Filmy select film).Count();
        }

        public void Dodaj(Film byt)
        {
            using (var kontekstBazyDanych = new Konteksty())
            {
                kontekstBazyDanych.Entry<Film>(byt);
                kontekstBazyDanych.SaveChanges();
            }
        }

        public void Aktualizuj(Film byt)
        {
            using (var kontekstBazyDanych = new Konteksty())
            {
                Film film = kontekstBazyDanych.Filmy.Where(m => m.IdFilmu == byt.IdFilmu).FirstOrDefault();
                if (film != null)
                {
                    kontekstBazyDanych.Entry(film).CurrentValues.SetValues(byt);
                    kontekstBazyDanych.SaveChanges();
                }
            }
        }

        public void UsunZaPomocaId(int id)
        {
            using (var kontekstBazyDanych = new Konteksty())
            {
                var film = kontekstBazyDanych.Filmy.Where(m => m.IdFilmu == id).FirstOrDefault();
                if (film != null)
                {
                    kontekstBazyDanych.Filmy.Remove(film);
                    kontekstBazyDanych.SaveChanges();
                }
            }
        }

        public void UsunWszystkie()
        {
            using (var kontekstBazyDanych = new Konteksty())
            {
                List<Film> wszystkieFilmy = ZwrocWszystkie();
                foreach (Film film in wszystkieFilmy)
                    kontekstBazyDanych.Filmy.Remove(film);

                kontekstBazyDanych.SaveChanges();
            }
        }
    }
}
