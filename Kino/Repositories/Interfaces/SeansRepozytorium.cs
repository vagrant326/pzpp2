﻿using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System;

namespace Kino.Repositories.Interfaces
{
    using Models;

    public abstract class SeansRepozytorium : IRepozytorium<Seans>
    {
        public List<Seans> ZwrocWszystkie()
        {
            using (var kontekstBazyDanych = new Konteksty())
            {
                /* http://stackoverflow.com/questions/9759928/foreign-key-is-null-while-it-is-not-when-using-linq */
                /* http://stackoverflow.com/questions/4206634/system-objectdisposedexception-the-objectcontext-instance-has-been-disposed-and */
                // uprościć i dodać to do wszystkich kontekstów inaczej dostaje od AJAX'a po stronie JavaScirpt'u: Internal Server Error a po stronie C# MVC System.Object.DisposedException
                kontekstBazyDanych.Configuration.LazyLoadingEnabled = false;
                return kontekstBazyDanych.Seans.Include(a => a.film).Include(b => b.sala).ToList();
            }
        }

        public List<string> GetErrorMessages()
        {
            return new List<string>();
        }

        public Seans ZwrocZaPomocaId(int id)
        {
            using (var kontekstBazyDanych = new Konteksty())
                return kontekstBazyDanych.Seans.Where(m => m.IdSeansu == id).FirstOrDefault();
        }

        public int Policz()
        {
            using (var kontekstBazyDanych = new Konteksty())
                return (from Seans in kontekstBazyDanych.Seans select Seans).Count();
        }

        public void Dodaj(Seans byt)
        {
            using (var kontekstBazyDanych = new Konteksty())
            {
                kontekstBazyDanych.Entry<Seans>(byt);
                kontekstBazyDanych.SaveChanges();
            }
        }

        public void Aktualizuj(Seans byt)
        {
            using (var kontekstBazyDanych = new Konteksty())
            {
                Seans Seans = kontekstBazyDanych.Seans.Where(m => m.IdSeansu == byt.IdSeansu).FirstOrDefault();
                if (Seans != null)
                {
                    kontekstBazyDanych.Entry(Seans).CurrentValues.SetValues(byt);
                    kontekstBazyDanych.SaveChanges();
                }
            }
        }

        public void UsunZaPomocaId(int id)
        {
            using (var kontekstBazyDanych = new Konteksty())
            {
                var Seans = kontekstBazyDanych.Seans.Where(m => m.IdSeansu == id).FirstOrDefault();
                if (Seans != null)
                {
                    kontekstBazyDanych.Seans.Remove(Seans);
                    kontekstBazyDanych.SaveChanges();
                }
            }
        }

        public void UsunWszystkie()
        {
            using (var kontekstBazyDanych = new Konteksty())
            {
                List<Seans> wszystkieSeansy = ZwrocWszystkie();
                foreach (Seans Seans in wszystkieSeansy)
                    kontekstBazyDanych.Seans.Remove(Seans);

                kontekstBazyDanych.SaveChanges();
            }
        }
    }
}

