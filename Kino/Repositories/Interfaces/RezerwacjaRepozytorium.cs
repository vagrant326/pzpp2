﻿using System.Collections.Generic;
using System.Linq;
using System;
using System.Data.Entity.Validation;

namespace Kino.Repositories.Interfaces
{
    using Models;

    public abstract class RezerwacjaRepozytorium : IRepozytorium<Rezerwacja>
    {
        List<string> errorMessages = new List<string>();

        public List<Rezerwacja> ZwrocWszystkie()
        {
            using (var kontekstBazyDanych = new Konteksty())
                return (from rezerwacja in kontekstBazyDanych.Rezerwacja select rezerwacja).ToList();
        }

        public List<string> GetErrorMessages()
        {
            return new List<string>();
        }

        public Rezerwacja ZwrocZaPomocaId(int id)
        {
            using (var kontekstBazyDanych = new Konteksty())
                return kontekstBazyDanych.Rezerwacja.Where(m => m.IdRezerwacji == id).FirstOrDefault();
        }

        public int Policz()
        {
            using (var kontekstBazyDanych = new Konteksty())
                return (from rezerwacja in kontekstBazyDanych.Rezerwacja select rezerwacja).Count();
        }

        public void Dodaj(Rezerwacja byt)
        {
            //using (var kontekstBazyDanych = new Konteksty())
            //{
            //    kontekstBazyDanych.Entry<Rezerwacja>(byt);
            //    kontekstBazyDanych.SaveChanges();
            //}
            try
            {
                using (var kontekstBazyDanych = new Konteksty())
                {
                    kontekstBazyDanych.Rezerwacja.Add(byt);
                    kontekstBazyDanych.SaveChanges();
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    string errorMessage = "Entity of type \"" + eve.Entry.Entity.GetType().Name + "\" in state \"" + eve.Entry.State + "\" has the following validation errors:\n";
                    foreach (var ve in eve.ValidationErrors)
                        errorMessage += "- Property: \"" + ve.PropertyName + "\", Error: \"" + ve.ErrorMessage + "\"\n";
                    errorMessages.Add(errorMessage);
                }
            }
        }

        public void Aktualizuj(Rezerwacja byt)
        {
            using (var kontekstBazyDanych = new Konteksty())
            {
                Rezerwacja rezerwacja = kontekstBazyDanych.Rezerwacja.Where(m => m.IdRezerwacji == byt.IdRezerwacji).FirstOrDefault();
                if (rezerwacja != null)
                {
                    kontekstBazyDanych.Entry(rezerwacja).CurrentValues.SetValues(byt);
                    kontekstBazyDanych.SaveChanges();
                }
            }
        }

        public void UsunZaPomocaId(int id)
        {
            using (var kontekstBazyDanych = new Konteksty())
            {
                var rezerwacja = kontekstBazyDanych.Rezerwacja.Where(m => m.IdRezerwacji == id).FirstOrDefault();
                if (rezerwacja != null)
                {
                    kontekstBazyDanych.Rezerwacja.Remove(rezerwacja);
                    kontekstBazyDanych.SaveChanges();
                }
            }
        }

        public void UsunWszystkie()
        {
            using (var kontekstBazyDanych = new Konteksty())
            {
                List<Rezerwacja> wszystkieRezerwacje = ZwrocWszystkie();
                foreach (Rezerwacja rezerwacja in wszystkieRezerwacje)
                    kontekstBazyDanych.Rezerwacja.Remove(rezerwacja);

                kontekstBazyDanych.SaveChanges();
            }
        }
    }
}
