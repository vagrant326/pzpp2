﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kino.Repositories.Interfaces
{
    using Models;

    public abstract class UzytkownikRepozytorium : IRepozytorium<Uzytkownik>
    {
        public List<Uzytkownik> ZwrocWszystkie()
        {
            using (var kontekstBazyDanych = new Konteksty())
                return (from uzytkownik in kontekstBazyDanych.Uzytkownicy select uzytkownik).ToList();
        }

        public Uzytkownik ZwrocZaPomocaId(int id)
        {
            using (var kontekstBazyDanych = new Konteksty())
                return kontekstBazyDanych.Uzytkownicy.Where(m => m.idUzytkownika == id).FirstOrDefault();
        }

        public List<string> GetErrorMessages()
        {
            return new List<string>();
        }

        public int Policz()
        {
            using (var kontekstBazyDanych = new Konteksty())
                return (from uzytkownik in kontekstBazyDanych.Uzytkownicy select uzytkownik).Count();
        }

        public void Dodaj(Uzytkownik byt)
        {
            using (var kontekstBazyDanych = new Konteksty())
            {
                kontekstBazyDanych.Entry<Uzytkownik>(byt);
                kontekstBazyDanych.SaveChanges();
            }
        }

        public void Aktualizuj(Uzytkownik byt)
        {
            using (var kontekstBazyDanych = new Konteksty())
            {
                Uzytkownik uzytkownik = kontekstBazyDanych.Uzytkownicy.Where(m => m.idUzytkownika == byt.idUzytkownika).FirstOrDefault();
                if (uzytkownik != null)
                {
                    kontekstBazyDanych.Entry(uzytkownik).CurrentValues.SetValues(byt);
                    kontekstBazyDanych.SaveChanges();
                }
            }
        }

        public void UsunZaPomocaId(int id)
        {
            using (var kontekstBazyDanych = new Konteksty())
            {
                var uzytkownik = kontekstBazyDanych.Uzytkownicy.Where(m => m.idUzytkownika == id).FirstOrDefault();
                if (uzytkownik != null)
                {
                    kontekstBazyDanych.Uzytkownicy.Remove(uzytkownik);
                    kontekstBazyDanych.SaveChanges();
                }
            }
        }

        public void UsunWszystkie()
        {
            using (var kontekstBazyDanych = new Konteksty())
            {
                List<Uzytkownik> wszyscyUzytkownicy = ZwrocWszystkie();
                foreach (Uzytkownik uzytkownik in wszyscyUzytkownicy)
                    kontekstBazyDanych.Uzytkownicy.Remove(uzytkownik);

                kontekstBazyDanych.SaveChanges();
            }
        }
    }
}
