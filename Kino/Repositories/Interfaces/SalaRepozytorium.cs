﻿using System.Collections.Generic;
using System.Linq;

namespace Kino.Repositories.Interfaces
{
    using Models;

    public abstract class SalaRepozytorium : IRepozytorium<Sala>
    {
        public List<Sala> ZwrocWszystkie()
        {
            using (var kontekstBazyDanych = new Konteksty())
                return (from sala in kontekstBazyDanych.Sala select sala).ToList();
        }

        public Sala ZwrocZaPomocaId(int id)
        {
            using (var kontekstBazyDanych = new Konteksty())
                return kontekstBazyDanych.Sala.Where(m => m.Numer == id).FirstOrDefault();
        }

        public List<string> GetErrorMessages()
        {
            return new List<string>();
        }

        public int Policz()
        {
            using (var kontekstBazyDanych = new Konteksty())
                return (from sala in kontekstBazyDanych.Sala select sala).Count();
        }

        public void Dodaj(Sala byt)
        {
            using (var kontekstBazyDanych = new Konteksty())
            {
                kontekstBazyDanych.Entry<Sala>(byt);
                kontekstBazyDanych.SaveChanges();
            }
        }

        public void Aktualizuj(Sala byt)
        {
            using (var kontekstBazyDanych = new Konteksty())
            {
                Sala sala = kontekstBazyDanych.Sala.Where(m => m.Numer == byt.Numer).FirstOrDefault();
                if (sala != null)
                {
                    kontekstBazyDanych.Entry(sala).CurrentValues.SetValues(byt);
                    kontekstBazyDanych.SaveChanges();
                }
            }
        }

        public void UsunZaPomocaId(int id)
        {
            using (var kontekstBazyDanych = new Konteksty())
            {
                var sala = kontekstBazyDanych.Sala.Where(m => m.Numer == id).FirstOrDefault();
                if (sala != null)
                {
                    kontekstBazyDanych.Sala.Remove(sala);
                    kontekstBazyDanych.SaveChanges();
                }
            }
        }

        public void UsunWszystkie()
        {
            using (var kontekstBazyDanych = new Konteksty())
            {
                List<Sala> wszystkieSale = ZwrocWszystkie();
                foreach (Sala sala in wszystkieSale)
                    kontekstBazyDanych.Sala.Remove(sala);

                kontekstBazyDanych.SaveChanges();
            }
        }
    }
}
