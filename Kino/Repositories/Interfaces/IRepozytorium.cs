﻿using System.Collections.Generic;

namespace Kino.Repositories.Interfaces
{
    public interface IRepozytorium<T>
    {
        List<T> ZwrocWszystkie();
        T ZwrocZaPomocaId(int id);
        int Policz();

        void Dodaj(T byt);

        void Aktualizuj(T byt);

        void UsunZaPomocaId(int id);
        void UsunWszystkie();

        List<string> GetErrorMessages();
    }
}
